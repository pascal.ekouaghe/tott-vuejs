(function() {
    Vue.component('todo-input', {
        // TODO onenter appeler addTodo
        template: '<input @keyup.enter="alert(\'enter\')" class="new-todo" autofocus autocomplete="off" placeholder="What needs to be done?">',
        props: ['todos'],
        methods: {
            addTodo: function (todoInput) {
                var todo = todoInput.value;
                // TODO ajouter le nouveau todo
                todoInput.value = '';
            }
        }
    });
})();